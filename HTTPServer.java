import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
 
public class HTTPServer {
	public static void main(String[] args) {
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(80, 10);
			while (true) {
				Socket socket = serverSocket.accept();
				InputStream is = socket.getInputStream();
				byte[] ch = new byte[2048];
				is.read(ch);
//				is.close();
				String out = "";
				try {
					String request = new String (ch);
					String get = request.substring(request.indexOf("GET")+2,request.indexOf(" HTTP/1.1"));
					
					if (get.indexOf("add") != -1 ||get.indexOf("mult") != -1) {
						int a = Integer.parseInt(request.substring(request.indexOf("a=")+2,request.indexOf("&")));
						int b = Integer.parseInt(request.substring(request.indexOf("b=")+2,request.indexOf(" HTTP/1.1")));
						if (get.indexOf("add") != -1) {
							out = (a+b)+"";
						}else {
							out = (a*b)+"";
						}
						System.out.println(out);
					}
				} catch (Exception e) {
					out = "parameter error";
				}

				
                /*发送回执*/
                PrintWriter pw = new PrintWriter(socket.getOutputStream());
 
                pw.println("HTTP/1.1 200 OK");
                pw.println("Content-type:text/html");
                pw.println();
                pw.println("<h1>"+out+"</h1>");
 
                pw.flush();
                socket.close();
				is.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}